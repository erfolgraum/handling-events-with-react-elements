import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
// import App from "./App";

const root = ReactDOM.createRoot(document.getElementById("root"));

const sliderBtn = (
  <label className="switch" htmlFor="checkbox">
    <input type="checkbox" id="checkbox" />
    <div
      className="slider round"
      onClick={() => {
        changeTheme();
      }}
    ></div>
  </label>
);

const body = document.querySelector("body");


const planetList = (
  <ul className="planets-list"
    // className={
    //   (body.classList.contains("dark")) 
    //   ? "active-color" 
    //   : "planets-list"
    // }
  >
    <li>Mercury</li>
    <li>Venus</li>
    <li>Earth</li>
    <li>Mars</li>
    <li>Jupiter</li>
    <li>Saturn</li>
    <li>Uranus</li>
    <li>Neptune</li>
  </ul>
);

root.render(
  React.createElement(
    "div",
    {},
    React.createElement(
      "h1",
      { style: { color: "#999", fontSize: "19px" } },
      "Solar system planets"
    ),
    planetList,
    sliderBtn
  )
);



const changeTheme = () => {
  body.classList.toggle("dark");
  const ul = document.querySelector('.planets-list')  
  
  body.classList.contains("dark") 
  ? ul.classList.add('active-color')
  : ul.classList.remove('active-color')
};

// active state #66bb6a
