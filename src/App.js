import React from "react";
import "./App.css";

function App() {
  return (
  <>
  <Title />
  <Slider />
    <ul className="planets-list">
      <li>Mercury</li>
      <li>Venus</li>
      <li>Earth</li>
      <li>Mars</li>
      <li>Jupiter</li>
      <li>Saturn</li>
      <li>Uranus</li>
      <li>Neptune</li>
    </ul>
  </>
  );
}

const Slider = ()=> {
  return (
    <label className="switch" htmlFor="checkbox">
        <input type="checkbox" id="checkbox" />
        <div className="slider round"></div>
    </label>
  )
}


const Title = ()=> {
  return React.createElement("h1", { style: { color: '#999', fontSize: '19px'}}, "Solar system planets")
}


export default App;
